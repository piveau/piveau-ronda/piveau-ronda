# piveau Ronda

## Functionality
The datastore can process real-time streaming data as well as static datasets.
It uses a Vertx Cluster to run Connector Verticles to fetch real-time data sources and a Webserver Verticle for users to upload datasets.
The incoming real-time data gets forwarded through Apache Kafka, processed with Spark Streaming and saved to an HDFS Cluster.
The static datasets can be uploaded to HDFS directly. A Http and Websocket Server is running also in Vertx to provide the processed data.

## Prerequisites
* Docker (Production)
* Docker-compose (Production)


## Build Instructions
To build and sart the project you follow these steps.

1. Fill in the missing API Keys and Passwords in the harvester.env and scheduler.env files
2. Start the Datastore by executing the following command in a shell: "docker-compose up -d"

## Usage
After following the build instructions you can access the following services in your browser

| Service| URL |
| :---  | :---        |
| Datastore | http://localhost:8080 |
| HDFS | http://localhost:9870 |
| Spark Cluster | http://localhost:8081 |
| Spark Worker | http://localhost:8082 |
| Spark App | http://localhost:4040 |

